package id.bima.diver.authentication.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import id.bima.diver.authentication.constant.AuthPath;
import id.bima.diver.authentication.dto.LoginRequestDto;
import id.bima.diver.authentication.dto.LoginResponseDto;
import id.bima.diver.authentication.dto.LogoutRequestDto;
import id.bima.diver.authentication.dto.LogoutResponseDto;
import id.bima.diver.authentication.dto.ValidateRequestDto;
import id.bima.diver.authentication.dto.ValidateResponseDto;
import id.bima.diver.authentication.service.LoginService;
import id.bima.diver.authentication.service.LogoutService;
import id.bima.diver.authentication.service.ValidateService;
import id.bima.diver.common.dto.BaseResponse;

@RestController
@RequestMapping(AuthPath.AUTH_V1)
public class AuthController implements AuthApi {
	
	private LoginService loginService;
	private LogoutService logoutService;
	private ValidateService validateService;
	
	@Autowired
	public AuthController(LoginService loginService, LogoutService logoutService, ValidateService validateService) {
		this.loginService = loginService;
		this.logoutService = logoutService;
		this.validateService = validateService;
	}

	@Override
	public BaseResponse<LoginResponseDto> mainLogin(LoginRequestDto request) {
		return loginService.mainLogin(request);
	}

	@Override
	public BaseResponse<LoginResponseDto> adminLogin(LoginRequestDto request) {
		return loginService.adminLogin(request);
	}
	
	@Override
	public BaseResponse<ValidateResponseDto> validate(ValidateRequestDto request) {
		return validateService.validate(request);
	}
	
	@Override
	public BaseResponse<LogoutResponseDto> logout(LogoutRequestDto request) {
		return logoutService.logout(request);
	}

}
