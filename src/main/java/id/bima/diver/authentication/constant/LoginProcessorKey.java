package id.bima.diver.authentication.constant;

public class LoginProcessorKey {

	private LoginProcessorKey() {
	}
	
	public static final String USER_AUTH = "userAuth";
	
}
