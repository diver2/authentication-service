package id.bima.diver.authentication.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LogoutRequestDto implements Serializable {

	private static final long serialVersionUID = -4901497287714401529L;
	
	private String token;
	
}
