package id.bima.diver.authentication.dto;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class LoginRequestDto implements Serializable {

	private static final long serialVersionUID = 1727060555188695252L;
	
	private String username;
	private String password;
	
}
