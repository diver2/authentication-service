package id.bima.diver.authentication.service;

import java.util.List;
import java.util.Objects;

import javax.crypto.SecretKey;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;
import org.springframework.util.CollectionUtils;

import id.bima.diver.authentication.constant.LoginProcessorKey;
import id.bima.diver.authentication.dto.LoginRequestDto;
import id.bima.diver.authentication.user.dto.UserAuthDto;
import id.bima.diver.authentication.user.service.UserAuthService;
import id.bima.diver.common.constant.ErrorEnum;
import id.bima.diver.common.constant.RedisKey;
import id.bima.diver.common.exception.GenericException;
import id.bima.diver.common.util.AesUtil;

@Component
@Scope("prototype")
public class DefaultLoginProcessor extends LoginProcessor {
	
	private static final long serialVersionUID = -4515431377190501507L;
	
	public static final String SECRET_KEY_STR = "XxrjEVDlC9ufOWBCF9hYfQjRNgDOIOmkUG5UfkjtDzQ=";
	
	private transient RedisService redisService;
	private transient UserAuthService userAuthService;
	
	@Autowired
	public DefaultLoginProcessor(RedisService redisService, UserAuthService userAuthService) {
		super();
		this.redisService = redisService;
		this.userAuthService = userAuthService;
	}

	@Override
	public void authenticate(LoginRequestDto req) throws GenericException {
		// Check request
		if (StringUtils.isBlank(req.getPassword())) {
			throw new GenericException(ErrorEnum.INVALID_CREDENTIAL);
		}

		// Check if username exist or not
		List<UserAuthDto> listAuth = userAuthService.getUserAuthByUserName(req.getUsername());
		if (CollectionUtils.isEmpty(listAuth)) {
			throw new GenericException(ErrorEnum.INVALID_CREDENTIAL);
		}
		
		// Check if password is valid
		UserAuthDto auth = listAuth.get(0);
		SecretKey secretKey = AesUtil.base64ToSecretKey(SECRET_KEY_STR);
		String savedPassword = AesUtil.decrypt(auth.getPassword(), secretKey);
		if (!req.getPassword().equals(savedPassword)) {
			throw new GenericException(ErrorEnum.INVALID_CREDENTIAL);
		}
		
		// Check if user already login or not
		String checkToken = redisService.getData(RedisKey.LOGIN_SESSION + ":" + req.getUsername());
		if (StringUtils.isNotBlank(checkToken)) {
			throw new GenericException(ErrorEnum.USER_ALREADY_LOGIN);
		}
		
		this.setInput(LoginProcessorKey.USER_AUTH, auth);
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		result = prime * result + Objects.hash(redisService, userAuthService);
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (getClass() != obj.getClass())
			return false;
		DefaultLoginProcessor other = (DefaultLoginProcessor) obj;
		return Objects.equals(redisService, other.redisService)
				&& Objects.equals(userAuthService, other.userAuthService);
	}
	
}
