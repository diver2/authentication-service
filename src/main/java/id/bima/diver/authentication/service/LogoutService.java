package id.bima.diver.authentication.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bima.diver.authentication.dto.LogoutRequestDto;
import id.bima.diver.authentication.dto.LogoutResponseDto;
import id.bima.diver.common.constant.ErrorEnum;
import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.util.BaseResponseUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LogoutService {
	
	private JwtService jwtService;
	
	@Autowired
	public LogoutService(JwtService jwtService) {
		this.jwtService = jwtService;
	}

	public BaseResponse<LogoutResponseDto> logout(LogoutRequestDto req) {
		log.info("Logout start");
		ErrorEnum rc = ErrorEnum.SUCCESS;
		LogoutResponseDto result = null;
		try {
			result = LogoutResponseDto.builder()
					.isSuccess(jwtService.revokeToken(req.getToken()))
					.build();
		} catch (Throwable e) {
			log.error("Error when login", e);
			rc = BaseResponseUtils.getErrorCode(e);
		} 

		return BaseResponseUtils.constructResponse(rc, result);
	}

}
