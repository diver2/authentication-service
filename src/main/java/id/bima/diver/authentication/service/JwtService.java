package id.bima.diver.authentication.service;

import java.security.Key;
import java.time.Duration;
import java.util.Date;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import id.bima.diver.authentication.parameter.dto.ParameterDto;
import id.bima.diver.authentication.parameter.service.ParameterService;
import id.bima.diver.common.constant.ErrorEnum;
import id.bima.diver.common.constant.GeneralParameterDefault;
import id.bima.diver.common.constant.RedisKey;
import id.bima.diver.common.exception.GenericException;
import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import io.jsonwebtoken.io.Decoders;
import io.jsonwebtoken.security.Keys;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class JwtService {

	@Value("${application.security.jwt.secret-key}")
	private String secretKey;
	
	private ParameterService parameterService;
	private RedisService redisService;
	
	@Autowired
	public JwtService(ParameterService parameterService, RedisService redisService) {
		this.parameterService = parameterService;
		this.redisService = redisService;
	}

	public String buildToken(String subject, String issuer, String id, Map<String, Object> claims) throws GenericException {
		
		log.info("Subject : {}, Issuer : {}, Id : {}", subject, issuer, id);
		String token = Jwts
				.builder()
				.setClaims(claims)
				.setSubject(subject)
				.setIssuer(issuer)
				.setId(id)
				.setIssuedAt(new Date(System.currentTimeMillis()))
				.signWith(getSignInKey(), SignatureAlgorithm.HS256)
				.compact();
		
		// Save token to redis
		ParameterDto expiredParam = parameterService.getByModuleAndKey(RedisKey.GENERAL, GeneralParameterDefault.TOKEN_EXPIRED_CACHE.name());
		Long expired = Long.valueOf(expiredParam.getValue1());
		redisService.setData(RedisKey.TOKEN + ":" + token, subject, Duration.ofMinutes(expired));
		redisService.setData(RedisKey.LOGIN_SESSION + ":" + subject, token, Duration.ofMinutes(expired));
		
		return token;
	}
	
	public Boolean revokeToken(String token) {
		String subject = redisService.getData(RedisKey.TOKEN + ":" + token);
		redisService.deleteKey(RedisKey.LOGIN_SESSION + ":" + subject);
		return redisService.deleteKey(RedisKey.TOKEN + ":" + token);
	}
	
	public Claims validateToken(String token) throws GenericException {
		ParameterDto expiredParam = parameterService.getByModuleAndKey(RedisKey.GENERAL, GeneralParameterDefault.TOKEN_EXPIRED_CACHE.name());
		Long expired = Long.valueOf(expiredParam.getValue1());
		
		// Check if token is valid
		String subject = redisService.getData(RedisKey.TOKEN + ":" + token, Duration.ofMinutes(expired));
		if (StringUtils.isBlank(subject)) {
			throw new GenericException(ErrorEnum.INVALID_TOKEN);
		}
		
		// Check if login session still available
		String checkToken = redisService.getData(RedisKey.LOGIN_SESSION + ":" + subject, Duration.ofMinutes(expired));
		if (StringUtils.isBlank(checkToken)) {
			throw new GenericException(ErrorEnum.INVALID_TOKEN);
		}
		
		return Jwts.parserBuilder().setSigningKey(getSignInKey()).build().parseClaimsJws(token).getBody();
	}
	
	private Key getSignInKey() {
		return Keys.hmacShaKeyFor(Decoders.BASE64.decode(secretKey));
	}

}
