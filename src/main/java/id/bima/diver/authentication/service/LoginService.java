package id.bima.diver.authentication.service;

import java.util.HashMap;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bima.diver.authentication.constant.LoginProcessorKey;
import id.bima.diver.authentication.constant.LoginType;
import id.bima.diver.authentication.dto.LoginRequestDto;
import id.bima.diver.authentication.dto.LoginResponseDto;
import id.bima.diver.authentication.user.dto.GetUserResponseDto;
import id.bima.diver.authentication.user.dto.UserAuthDto;
import id.bima.diver.authentication.user.service.UserService;
import id.bima.diver.common.constant.ErrorEnum;
import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.util.BaseResponseUtils;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class LoginService {
	
	private LoginProcessorFactory loginProcessorFactory;
	private JwtService jwtService;
	private UserService userService;
	
	@Autowired
	public LoginService(LoginProcessorFactory loginProcessorFactory, JwtService jwtService, UserService userService) {
		this.loginProcessorFactory = loginProcessorFactory;
		this.jwtService = jwtService;
		this.userService = userService;
	}

	public BaseResponse<LoginResponseDto> mainLogin(LoginRequestDto req) {
		return login(req, LoginType.MAIN);
	}
	
	public BaseResponse<LoginResponseDto> adminLogin(LoginRequestDto req) {
		return login(req, LoginType.ADMIN);
	}
	
	private BaseResponse<LoginResponseDto> login(LoginRequestDto req, LoginType type) {
		log.info("Login {} start", type.name());
		ErrorEnum rc = ErrorEnum.SUCCESS;
		LoginResponseDto result = null;
		LoginProcessor processor = null;
		
		try {
			// Authenticate
			processor = loginProcessorFactory.getProcessorInstance();
			processor.authenticate(req);
			
			// Create token
			UserAuthDto userAuth = (UserAuthDto) processor.get(LoginProcessorKey.USER_AUTH);
			String token = jwtService.buildToken(userAuth.getUserName(), type.name(), 
					userAuth.getUserId().toString(), new HashMap<>());
			
			// Get response data
			GetUserResponseDto userData = userService.getUserFullById(userAuth.getUserId());
			result = LoginResponseDto.builder()
					.token(token)
					.user(userData.getUser())
					.listRole(userData.getListRole())
					.listUserGroup(userData.getListUserGroup())
					.additionalData(processor.getAdditionalData())
					.build();
			log.info("Login process finished");
		} catch (Throwable e) {
			log.error("Error when login", e);
			rc = BaseResponseUtils.getErrorCode(e);
		} finally {
			if (processor != null) {
				loginProcessorFactory.destroyProcessor(processor);
			}
		}
		
		return BaseResponseUtils.constructResponse(rc, result);
	}
	
}
