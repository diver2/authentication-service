package id.bima.diver.authentication.service;

import org.springframework.beans.BeansException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Component;

import id.bima.diver.authentication.parameter.dto.ParameterDto;
import id.bima.diver.authentication.parameter.service.ParameterService;
import id.bima.diver.common.constant.GeneralParameterDefault;
import id.bima.diver.common.constant.RedisKey;
import id.bima.diver.common.exception.GenericException;

@Component
public class LoginProcessorFactory {
	
	private ApplicationContext appContext;
	private ParameterService parameterService;
	
	@Autowired
	public LoginProcessorFactory(ApplicationContext appContext, ParameterService parameterService) {
		this.appContext = appContext;
		this.parameterService = parameterService;
	}

	public LoginProcessor getProcessorInstance() throws BeansException, IllegalStateException, ClassNotFoundException, GenericException {
		ParameterDto procParam = parameterService.getByModuleAndKey(RedisKey.GENERAL, GeneralParameterDefault.LOGIN_PROCESSOR.name());
		return (LoginProcessor) appContext.getAutowireCapableBeanFactory().createBean(Class.forName(procParam.getValue1()));
	}

	public void destroyProcessor(LoginProcessor processor) {
		appContext.getAutowireCapableBeanFactory().destroyBean(processor);
	}
	
}
