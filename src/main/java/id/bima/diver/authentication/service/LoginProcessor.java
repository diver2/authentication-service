package id.bima.diver.authentication.service;

import java.util.Collections;
import java.util.Map;

import org.springframework.stereotype.Component;

import id.bima.diver.authentication.dto.LoginRequestDto;
import id.bima.diver.common.exception.GenericException;
import id.bima.diver.common.service.MapProcessor;

@Component
public abstract class LoginProcessor extends MapProcessor {
	
	private static final long serialVersionUID = -8245441027825872722L;

	public abstract void authenticate(LoginRequestDto req) throws GenericException;
	
	/*
	 * This method used to set any additional data that needed as response.
	 * Please override this method on respective login processor if you want to set any additional data for login api response.
	 */
	public Map<String, Object> getAdditionalData() {
		return Collections.emptyMap();
	}
	
}
