package id.bima.diver.authentication.service;

import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;

import id.bima.diver.authentication.dto.ValidateRequestDto;
import id.bima.diver.authentication.dto.ValidateResponseDto;
import id.bima.diver.authentication.parameter.dto.ParameterDto;
import id.bima.diver.authentication.parameter.service.ParameterService;
import id.bima.diver.authentication.user.dto.FunctionDto;
import id.bima.diver.authentication.user.dto.GetUserResponseDto;
import id.bima.diver.authentication.user.service.UserService;
import id.bima.diver.common.constant.ErrorEnum;
import id.bima.diver.common.constant.GeneralParameterDefault;
import id.bima.diver.common.constant.RedisKey;
import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.exception.GenericException;
import id.bima.diver.common.util.BaseResponseUtils;
import id.bima.diver.common.util.JsonUtils;
import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class ValidateService {

	private JwtService jwtService;
	private ParameterService parameterService;
	private UserService userService;
	
	@Autowired
	public ValidateService(JwtService jwtService, ParameterService parameterService, UserService userService) {
		super();
		this.jwtService = jwtService;
		this.parameterService = parameterService;
		this.userService = userService;
	}

	public BaseResponse<ValidateResponseDto> validate(ValidateRequestDto req) {
		log.info("Validate start");
		ErrorEnum rc = ErrorEnum.SUCCESS;
		ValidateResponseDto result = null;
		
		try {
			// Validate token
			Claims claims = validateToken(req.getToken(), req.getListRequiredType());
			
			// Validate function
			log.info(JsonUtils.objectAsStringJson(claims));
			Long userId = Long.valueOf(claims.getId());
			validateFunction(userId, req.getUrl());
			
			// Construct reponse
			result = ValidateResponseDto.builder()
					.userId(userId)
					.build();
		} catch (Throwable e) {
			log.error("Error when validate", e);
			rc = BaseResponseUtils.getErrorCode(e);
		} 

		return BaseResponseUtils.constructResponse(rc, result);
	}
	
	private Claims validateToken(String token, List<String> listRequiredType) throws GenericException {
		if (CollectionUtils.isEmpty(listRequiredType)) {
			throw new GenericException(ErrorEnum.INVALID_INPUT, "Type token is empty");
		}
		
		Claims claims = jwtService.validateToken(token);
		if (!listRequiredType.contains(claims.getIssuer())) {
			throw new GenericException(ErrorEnum.INVALID_TOKEN, "Token have different type");
		}
		
		return claims;
	}
	
	private void validateFunction(Long userId, String url) throws GenericException {
		ParameterDto paramValidateFunction = parameterService.getByModuleAndKey(RedisKey.GENERAL, GeneralParameterDefault.NEED_VALIDATE_FUNCTION.name());
		if (Boolean.parseBoolean(paramValidateFunction.getValue1())) {
			if (StringUtils.isBlank(url)) {
				throw new GenericException(ErrorEnum.RESTRICTED_ACCESS, "URL empty");
			}
			
			GetUserResponseDto userData = userService.getUserFullById(userId);
			if (!CollectionUtils.isEmpty(userData.getListFunction())) {
				for (FunctionDto functionDto : userData.getListFunction()) {
					if (url.equals(functionDto.getValue())) {
						return;
					}
				}
			}
			
			throw new GenericException(ErrorEnum.RESTRICTED_ACCESS, "User not have any required access");
		}
		
	}
	
}
