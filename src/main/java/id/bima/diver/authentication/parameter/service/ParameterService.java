package id.bima.diver.authentication.parameter.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bima.diver.authentication.parameter.dto.GetParameterRequestDto;
import id.bima.diver.authentication.parameter.dto.GetParameterResponseDto;
import id.bima.diver.authentication.parameter.dto.ParameterDto;
import id.bima.diver.authentication.service.RedisService;
import id.bima.diver.common.constant.RedisKey;
import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.exception.GenericException;

@Service
public class ParameterService {
	
	private ParameterClient parameterClient;
	private RedisService redisService;
	
	@Autowired
	public ParameterService(ParameterClient parameterClient, RedisService redisService) {
		this.parameterClient = parameterClient;
		this.redisService = redisService;
	}

	public ParameterDto getByModuleAndKey(String module, String key) throws GenericException {
		ParameterDto param = redisService.getDataFromJsonString(RedisKey.PARAMETER + ":" + module + ":" + key, ParameterDto.class);
		if (param == null) {
			GetParameterRequestDto request = GetParameterRequestDto.builder()
					.module(module)
					.key(key)
					.build();
			BaseResponse<GetParameterResponseDto> response = parameterClient.getByModuleAndKey(request);
			param = response.getData().getParameter();
		}
		
		return param;
	}
	
}
