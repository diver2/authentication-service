package id.bima.diver.authentication.user.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import id.bima.diver.authentication.user.constant.UserPath;
import id.bima.diver.authentication.user.dto.GetUserAuthRequestDto;
import id.bima.diver.authentication.user.dto.GetUserAuthResponseDto;
import id.bima.diver.common.dto.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;

@Lazy
@FeignClient(value = UserPath.USER_AUTH_V1, url = "${url.user-service:}" + UserPath.USER_AUTH_V1)
public interface UserAuthClient {
	
	@Operation(summary = "API to get user auth list by user name from database")
	@PostMapping(value = UserPath.GET_LIST_BY_USER_NAME, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetUserAuthResponseDto> getListByUserName(@RequestBody GetUserAuthRequestDto request);
	
	@Operation(summary = "API to get user auth list by user name and is deleted from database")
	@PostMapping(value = UserPath.GET_LIST_BY_USER_NAME_AND_IS_DELETED, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetUserAuthResponseDto> getListByUserNameAndIsDeleted(@RequestBody GetUserAuthRequestDto request);
	
	
}
