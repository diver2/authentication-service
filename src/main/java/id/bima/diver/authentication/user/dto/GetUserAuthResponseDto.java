package id.bima.diver.authentication.user.dto;

import java.io.Serializable;
import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class GetUserAuthResponseDto implements Serializable {
	
	private static final long serialVersionUID = 31482484063516502L;
	
	private List<UserAuthDto> listUserAuth;
	
}
