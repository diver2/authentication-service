package id.bima.diver.authentication.user.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bima.diver.authentication.user.dto.GetUserAuthRequestDto;
import id.bima.diver.authentication.user.dto.GetUserAuthResponseDto;
import id.bima.diver.authentication.user.dto.UserAuthDto;
import id.bima.diver.common.dto.BaseResponse;

@Service
public class UserAuthService {

	private UserAuthClient userAuthClient;
	
	@Autowired
	public UserAuthService(UserAuthClient userAuthClient) {
		this.userAuthClient = userAuthClient;
	}

	public List<UserAuthDto> getUserAuthByUserName(String userName) {
		GetUserAuthRequestDto request = GetUserAuthRequestDto.builder()
				.userName(userName)
				.isDeleted(false)
				.build();
		BaseResponse<GetUserAuthResponseDto> response = userAuthClient.getListByUserNameAndIsDeleted(request);
		return response.getData().getListUserAuth();
	}
	
}
