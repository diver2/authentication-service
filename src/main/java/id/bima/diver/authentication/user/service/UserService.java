package id.bima.diver.authentication.user.service;

import java.time.Duration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import id.bima.diver.authentication.parameter.dto.ParameterDto;
import id.bima.diver.authentication.parameter.service.ParameterService;
import id.bima.diver.authentication.service.RedisService;
import id.bima.diver.authentication.user.dto.GetUserRequestDto;
import id.bima.diver.authentication.user.dto.GetUserResponseDto;
import id.bima.diver.common.constant.GeneralParameterDefault;
import id.bima.diver.common.constant.RedisKey;
import id.bima.diver.common.dto.BaseResponse;
import id.bima.diver.common.exception.GenericException;

@Service
public class UserService {
	
	private UserClient userClient;
	private ParameterService parameterService;
	private RedisService redisService;
	
	@Autowired
	public UserService(UserClient userClient, ParameterService parameterService, RedisService redisService) {
		super();
		this.userClient = userClient;
		this.parameterService = parameterService;
		this.redisService = redisService;
	}

	public GetUserResponseDto getUserFullById(Long id) throws GenericException {
		// Get cache expired
		ParameterDto param = parameterService.getByModuleAndKey(RedisKey.GENERAL, GeneralParameterDefault.EXPIRED_CACHE.name());
		Long expired = Long.valueOf(param.getValue1());
		
		// Get user data
		GetUserResponseDto userData = redisService.getDataFromJsonString(RedisKey.USER_DATA + ":" + id, GetUserResponseDto.class, Duration.ofMinutes(expired));
		if (userData == null) {
			GetUserRequestDto request = GetUserRequestDto.builder()
					.id(id)
					.build();
			BaseResponse<GetUserResponseDto> response = userClient.getUserFullById(request);
			userData = response.getData();
		}
		
		return userData;
	}

}
