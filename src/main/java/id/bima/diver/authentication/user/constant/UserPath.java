package id.bima.diver.authentication.user.constant;

public class UserPath {

	private UserPath() {
	}
	
	// Main Path
	public static final String USER_V1 = "v1/user";
	public static final String USER_AUTH_V1 = "v1/user-auth";
	
	// Sub Path
	public static final String GET_LIST_BY_USER_NAME = "/get-list-by-user-name";
	public static final String GET_LIST_BY_USER_NAME_AND_IS_DELETED = "/get-list-by-user-name-and-is-deleted";
	public static final String GET_USER_FULL = "/get-user-full";
	
}
