package id.bima.diver.authentication.user.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.context.annotation.Lazy;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;

import id.bima.diver.authentication.user.constant.UserPath;
import id.bima.diver.authentication.user.dto.GetUserRequestDto;
import id.bima.diver.authentication.user.dto.GetUserResponseDto;
import id.bima.diver.common.dto.BaseResponse;
import io.swagger.v3.oas.annotations.Operation;

@Lazy
@FeignClient(value = UserPath.USER_V1, url = "${url.user-service:}" + UserPath.USER_V1)
public interface UserClient  {
	
	@Operation(summary = "API to get user data by user id from database")
	@PostMapping(value = UserPath.GET_USER_FULL, produces = { MediaType.APPLICATION_JSON_VALUE }, consumes = {MediaType.APPLICATION_JSON_VALUE })
	public BaseResponse<GetUserResponseDto> getUserFullById(@RequestBody GetUserRequestDto request);
	
}
